# Odoo Addons Test

A container to run tests of Odoo v13 addons based on python v3.8.12

## Usage:

In your addons repo `.gitlab-ci.yml`

```yaml
image: registry.gitlab.com/librecoop/odoo-addons-test:13
stages:
  - test

variables:
  POSTGRES_DB: project_ci_test
  POSTGRES_USER: odoo
  POSTGRES_PASSWORD: "odoo"

test:
  stage: test
  services:
    - postgres:10
  tags:
    - postgres
    - docker
  variables:
    LINT_CHECK: "0"
    TESTS: "1"
  script:
    - for addon in $(ls setup/ -I README -I _metapackage) ; do addon_dist="odoo13-addon-${addon}" ; echo "-e file://${PWD}/setup/${addon}#egg=${addon_dist}" >> test-requirements.txt ; done # install this repos addons
    - pip install --pre -r test-requirements.txt --index-url https://wheelhouse.odoo-community.org/oca-simple-and-pypi/
    - ${HOME}/maintainer-quality-tools/travis/travis_run_tests
    - ${HOME}/maintainer-quality-tools/travis/travis_after_tests_success || true # ignore codecov error
```