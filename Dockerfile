FROM python:3.8.12-slim

ENV VERSION="13.0"
ENV ODOO_BRANCH="13.0"
ENV ODOO_REPO="coopdevs/ocb"
ENV REPO_PLATFORM="gitlab"
ENV MQT_DEP="pip"

RUN apt-get update
RUN apt-get install -y sudo postgresql-client expect-dev python3-lxml python-dev python3-pip build-essential libsasl2-dev python-dev libldap2-dev libssl-dev libxmlsec1-dev gcc g++ make git curl wget libpq-dev libjpeg-dev zlib1g-dev
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get update
RUN apt-get install -y nodejs
RUN pip install --upgrade pip

RUN git clone https://github.com/coopdevs/maintainer-quality-tools.git -b feature/use-pip-install-deps ${HOME}/maintainer-quality-tools
RUN ${HOME}/maintainer-quality-tools/travis/travis_install_nightly
RUN cd /root/ocb-${ODOO_BRANCH} && python setup.py install && cd -
